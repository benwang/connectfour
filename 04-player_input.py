# Simple pygame program

# Import pygame.locals for easier access to key coordinates
# Updated to conform to flake8 and black standards
import pygame
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)


#################################################################
# Configuration
#################################################################
NUM_ROWS = 6
NUM_COLS = 7
CHIP_SIZE = 80
CHIP_PADDING = 10
BOARD_PADDING_LEFT = 2 * CHIP_SIZE
BOARD_PADDING_TOP = 2 * CHIP_SIZE

#################################################################
# Game variables
#################################################################

currPlayer = 1  # Either 1 or 2 for current player number
cursorPos = 0   # Index of player's chip cursor (0 through NUM_COLS - 1)
running = True  # Set this to false to exit the game

#################################################################
# Game Functions
#################################################################
def drawBoard():
    global board

    for r in range(NUM_ROWS):
        for c in range(NUM_COLS):
            chipX = (c * (CHIP_SIZE + CHIP_PADDING)) + BOARD_PADDING_LEFT
            chipY = (r * (CHIP_SIZE + CHIP_PADDING)) + BOARD_PADDING_TOP

            chipRect = (
                chipX - (CHIP_SIZE + CHIP_PADDING) / 2,
                chipY - (CHIP_SIZE + CHIP_PADDING) / 2,
                (CHIP_SIZE + CHIP_PADDING),
                (CHIP_SIZE + CHIP_PADDING)
            )

            pygame.draw.rect(screen, (255, 255, 255), chipRect, 5)
            if board[r][c] == 1:
                pygame.draw.circle(screen, (0, 0, 255), (chipX, chipY), CHIP_SIZE / 2)
            elif board[r][c] == 2:
                pygame.draw.circle(screen, (255, 0, 0), (chipX, chipY), CHIP_SIZE / 2)

def drawCursor():
    global currPlayer, cursorPos

    chipX = (cursorPos * (CHIP_SIZE + CHIP_PADDING)) + BOARD_PADDING_LEFT
    chipY = (-1 * (CHIP_SIZE + CHIP_PADDING)) + BOARD_PADDING_TOP

    if currPlayer == 1:
        pygame.draw.circle(screen, (0, 0, 255), (chipX, chipY), CHIP_SIZE / 2)
    else:
        pygame.draw.circle(screen, (255, 0, 0), (chipX, chipY), CHIP_SIZE / 2)


def processInput():
    global currPlayer, cursorPos, running
    
    # Look at every event in the queue
    for event in pygame.event.get():
        # Did the user hit a key?
        if event.type == KEYDOWN:
            # Was it the Escape key? If so, stop the loop.
            if event.key == K_ESCAPE:
                running = False
            
            # Move cursor left and right
            elif event.key == K_LEFT and cursorPos > 0:
                cursorPos -= 1
            elif event.key == K_RIGHT and cursorPos < NUM_COLS - 1:
                cursorPos += 1
            
            # Drop chip
            elif event.key == K_DOWN:
                print("TODO: Dropping chip!")

        # Did the user click the window close button? If so, stop the loop.
        elif event.type == QUIT:
            running = False

#################################################################
# Main program
#################################################################

# Example board
board = [[0]*NUM_COLS for row in range(NUM_ROWS)]
print(board)

# Fill bottom row with alternating colors
for c in range(NUM_COLS):
    board[NUM_ROWS - 1][c] = c % 3 # alternates 0,1,2

# Initialize the pygame library
pygame.init()

# Set up the drawing window
screen = pygame.display.set_mode([1280, 768])

xVal = 250

# Run until the user asks to quit
while running:
    # Process input
    processInput()

    # Fill the background with white
    # screen.fill((255, 255, 255))
    screen.fill((10, 10, 10))

    # Draw the game
    drawBoard()
    drawCursor()

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()
