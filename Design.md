# Connect 4 Design

## Architecture

- Initialization
  - Set board to empty
  - Determine player or AI
    - Easy implementation: hard coded value
    - Better implementation: command line argument
    - Best implementation: in-game setting
- Main loop
  - Check for external events:
    - Window closed
    - Game reset button
    - (Optional) AI/human player 2 toggle
  - Switch depending on player
    - If regular user:
      - Get user input, update board
        - Display any errors
        - Toggle next player
    - Else, if AI is enabled, call AI logic
      - AI makes turn
    - Toggle current player
  - Check for win
    - Display winner
  - Draw graphics
    - Draw board
      - Draw grid
      - Draw game chips
    - Draw buttons
    - Draw informational displays
      - Current player number
      - Winner?

## Data structures

- Board: 6x7 2D array
  - 0 for no one
  - 1 for P1
  - 2 for P2
- Game info
  - Current player number
  - Is P2 bot?

## Functions

### Required

- Get user input
- Check for valid move
- Toggle user/player
- Check for winner

### Optional

- AI logic - processAI()
- DEBUG: Print game state
- Add animations

## Questions

- How do we want to input column when it's your turn?
  - Draw a chip above column 0. Use arrow keys to move it. Hit enter/space to drop.
  - Use mouse to click column (can optionally draw chip above the board on mouseover)
- Decide approach:
  1. Write game logic, then add front end
  2. Write front end, then back end