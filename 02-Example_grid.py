# Simple pygame program

NUM_ROWS = 6
NUM_COLS = 7
CHIP_SIZE = 80
CHIP_PADDING = 10
BOARD_PADDING_LEFT = 2 * CHIP_SIZE
BOARD_PADDING_TOP = 2 * CHIP_SIZE

# Import and initialize the pygame library
import pygame
pygame.init()

# Set up the drawing window
screen = pygame.display.set_mode([1280, 768])

xVal = 250

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Fill the background with white
    # screen.fill((255, 255, 255))
    screen.fill((10, 10, 10))

    # Draw the board
    for r in range(NUM_ROWS):
        for c in range(NUM_COLS):
            chipX = (c * (CHIP_SIZE + CHIP_PADDING)) + BOARD_PADDING_LEFT
            chipY = (r * (CHIP_SIZE + CHIP_PADDING)) + BOARD_PADDING_TOP

            chipRect = (
                chipX - (CHIP_SIZE + CHIP_PADDING) / 2,
                chipY - (CHIP_SIZE + CHIP_PADDING) / 2,
                (CHIP_SIZE + CHIP_PADDING),
                (CHIP_SIZE + CHIP_PADDING)
            )

            pygame.draw.rect(screen, (255, 255, 255), chipRect, 5)
            pygame.draw.circle(screen, (0, 0, 255), (chipX, chipY), CHIP_SIZE / 2)

    xVal = (xVal + 1) % 500

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()
